package nsp.youtubezadecu;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import nsp.youtubezadecu.R;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import static nsp.youtubezadecu.Fragment_restrictriction.REST_DAY;

public class RestrictionActivity extends AppCompatActivity {
    public static String MESSAGE_DAY    = "rest day";
    public static String MESSAGE_NIGHT  = "rest night";
    public static String MESSAGE_ATONCE = "rest atonce";

    private ArrayList<YoutubeClass> mYoutubePlaylistsList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest);

        Intent intent = getIntent();
        short selector = intent.getShortExtra("selector", REST_DAY);
        mYoutubePlaylistsList = intent.getParcelableArrayListExtra("mYoutubePlaylistsList");

        Fragment_restrictriction fragment = Fragment_restrictriction.newInstance(selector);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_home_placeholder_rest, fragment);
        ft.addToBackStack("fragmentRest");
        ft.commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
        else{
            finishAffinity();
        }
    }

    ////////////////////////////////// todo duplikati iz mainAct
    public void onClick_langs(View view) {
        FragmentSettings.onClick_langs(view, this);
    }
    public void onClick_ages(View view) {
        FragmentSettings.onClick_ages(view, this);
    }
    public void clickInfoLangs(View view) {
        FragmentSettings.clickInfoLangs(view, this);
    }
    public void clickInfoAges(View view) {
        FragmentSettings.clickInfoAges(view, this);
    }
///////////////////////////////////////////////////////////////////////////

}
