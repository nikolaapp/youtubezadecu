package nsp.youtubezadecu;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import nsp.youtubezadecu.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class YoutubeFragment extends Fragment {

    private static String VIDEO_ID;
    private ArrayList<YoutubeClass> mYoutubeVideoList;
    private RecyclerView recyclerView;
    private androidx.recyclerview.widget.RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView mTextView;
    private YouTubePlayer mYouTubePlayer;
    private StaticData.Firebase mFirebase;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MainActivity.testAtonceRestriction(getActivity());
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View view = inflater.inflate(R.layout.fragment_video_player, container, false);

        recyclerView = view.findViewById(R.id.recycler_videos);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mTextView = view.findViewById(R.id.textViewVideoTitle);

        Bundle bundle = getArguments();
        mYoutubeVideoList = bundle.getParcelableArrayList("youtubeVideoList");
        VIDEO_ID = mYoutubeVideoList.get(0).getId();
        mTextView.setText(mYoutubeVideoList.get(0).getTitle());
        Toast.makeText(getActivity(), "ok", Toast.LENGTH_SHORT).show();

        YouTubePlayerView youTubePlayerView = view.findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youTubePlayerView);

        YouTubePlayerListener youTubePlayerListener = new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                mYouTubePlayer = youTubePlayer;
                Toast.makeText(getActivity(), VIDEO_ID, Toast.LENGTH_SHORT).show();
                youTubePlayer.loadVideo(VIDEO_ID, 0);
            }
        };

        IFramePlayerOptions iFramePlayerOptions = new IFramePlayerOptions.Builder()
                .controls(0)
                .rel(0)
                .ccLoadPolicy(0)
                .ivLoadPolicy(3)
                .build();
        youTubePlayerView.initialize(youTubePlayerListener, true, iFramePlayerOptions);

        mFirebase = new StaticData.Firebase(getActivity());

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar_player);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mAdapter = new PlaylistAdapter(getActivity(), mYoutubeVideoList, new OnPlaylistClickListener() {
            @Override
            public void onPlaylistClick(YoutubeClass item, int postion) {
                //Toast.makeText(getActivity(), item.getTitle(), Toast.LENGTH_SHORT).show();
                MainActivity.testAtonceRestriction(getActivity());
                mTextView.setText(item.getTitle());
                VIDEO_ID = item.getId();
                if (mYouTubePlayer != null) mYouTubePlayer.loadVideo(VIDEO_ID, 0);
                mFirebase.sendYoutubeData(item, postion);
            }
        });
        recyclerView.setAdapter(mAdapter);
    }

}
