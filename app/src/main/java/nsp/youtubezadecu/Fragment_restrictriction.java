package nsp.youtubezadecu;

import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import nsp.youtubezadecu.R;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class Fragment_restrictriction extends Fragment {
    public static final short REST_DAY = 1;
    public static final short REST_NIGHT = 2;
    public static final short REST_ATONCE = 3;

    private short selector;
    private RestClass restClass;
    private MediaPlayer mediaPlayer;

    AlertDialog mAlertDialog;
    View mAlertDialogView;

    public static Fragment_restrictriction newInstance(short message) {
        Bundle bundle = new Bundle();
        bundle.putShort("selector", message );
        Fragment_restrictriction fragment = new Fragment_restrictriction();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View view = inflater.inflate(R.layout.fragment_restriction, viewGroup, false);

        Bundle bundle = getArguments();
        selector = bundle.getShort("selector");

        restClass = new RestClass(selector);

        bundle = new Bundle();
        bundle.putString(restClass.name+" TAG", restClass.name);//fixme
        FirebaseAnalytics.getInstance(getActivity()).logEvent("selected_restriction", bundle);


        //prikaziToast(getActivity(), "Fragment_restrictriction - onCreateView");

        return view;
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        FloatingActionButton floatingActionButton = view.findViewById(R.id.floating_action_button_rest);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickedOnSettings();
            }
        });

        TextView textView = view.findViewById(R.id.restriction_message_rest);
        textView.setText(restClass.message);
        textView.setTextColor(getResources().getColor(restClass.textColor));

        ImageView imageView = view.findViewById(R.id.image_view);
        imageView.setImageResource(restClass.image);
        imageView.setScaleType(restClass.scale);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        mAlertDialogView = layoutInflaterAndroid.inflate(R.layout.enter_settings_dialog, null);
        builder.setView(mAlertDialogView);
        builder.setCancelable(true);
        mAlertDialog= builder.create();

        if (restClass.music!=0){
            mediaPlayer = MediaPlayer.create(getActivity(), restClass.music);
            mediaPlayer.start();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mediaPlayer != null){
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    private class RestClass{
        private String message, name;
        private int image, music, textColor;
        private ImageView.ScaleType scale;
        RestClass(short selector){
            switch (selector){
                case REST_DAY:
                    name="day_rest";
                    message="DOSTA ZA DANAS\nДОСТА ЗА ДАНАС";
                    textColor=R.color.colorPrimaryDark;
                    image = R.drawable.day;
                    scale = ImageView.ScaleType.CENTER_CROP;
                    music = R.raw.dosta_za_danas;
                    break;

                case REST_NIGHT:
                    name="night_rest";
                    message="LAKU NOĆ\nЛАКУ НОЋ";
                    textColor=R.color.colorAccent;
                    image = R.drawable.laku_noc;
                    scale = ImageView.ScaleType.CENTER_CROP;
                    music = R.raw.laku_noc;
                    break;

                case REST_ATONCE:
                    name="atonce_rest";
                    message="ODMOR\nОДМОР";
                    textColor=R.color.colorPrimaryDark;
                    image = R.drawable.day;
                    scale = ImageView.ScaleType.CENTER;
                    music = R.raw.vreme_za_pauzu;
                    break;
            }
        }

    }


    private short clicked = 0;
    private void clickedOnSettings() {
        if (clicked < StaticData.getClickToUnlockNum()) {
            StaticData.prikaziToast(getActivity(), "klikni brzo " + (StaticData.getClickToUnlockNum() - clicked) + " puta");
            clicked++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    clicked = 0;
                }
            }, 2000);
        } else if (clicked == StaticData.getClickToUnlockNum()) {
            FragmentPlaylists.showDialogAfterClicking(getActivity(), mAlertDialog, mAlertDialogView, R.id.fragment_home_placeholder_rest);
        }
    }

}
