package nsp.youtubezadecu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import nsp.youtubezadecu.R;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlalistViewHolder> {

    private ArrayList<YoutubeClass> mDataSet;
    private Context mContext;
    private final OnPlaylistClickListener mPlaylistListener;


    public PlaylistAdapter(Context mContext, ArrayList<YoutubeClass> dataSet, OnPlaylistClickListener playlistListener) {
        this.mDataSet = dataSet;
        this.mContext = mContext;
        this.mPlaylistListener = playlistListener;
    }

    public class PlalistViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        //private TextView textViewDes;
        //private TextView textViewDate;
        private ImageView ImageThumb;

        private PlalistViewHolder(View view) {
            super(view);
            this.textViewTitle = view.findViewById(R.id.textViewTitle);
            //this.textViewDes =   view.findViewById(R.id.textViewDes);
            //this.textViewDate =  view.findViewById(R.id.textViewDate);
            this.ImageThumb =    view.findViewById(R.id.ImageThumb);
        }

        private void bind(final YoutubeClass item, final OnPlaylistClickListener playlistClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playlistClickListener.onPlaylistClick(item, getLayoutPosition());
                }
            });
        }
    }


    @Override
    public PlaylistAdapter.PlalistViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.youtube_playlist_layout,parent,false);
        return new PlalistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PlalistViewHolder holder, int position) {
        YoutubeClass object = mDataSet.get(position);
        holder.textViewTitle.setText(object.getTitle());
        //holder.textViewDes.setText(object.getDescription());
        //holder.textViewDate.setText(object.getPublishedAt());
        holder.bind(mDataSet.get(position), mPlaylistListener);
        Picasso.get().load(object.getThumbnail()).into(holder.ImageThumb);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


}
