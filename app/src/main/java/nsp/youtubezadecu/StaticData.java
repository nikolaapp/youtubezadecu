package nsp.youtubezadecu;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.Random;

import static android.widget.Toast.LENGTH_SHORT;

public abstract class StaticData {

    private static String getApiKey(){
        return BuildConfig.IS_RELEASE ? API_KEY_release  : API_KEY_debug;
    }

    public static final String PRVO_POKRETANJE =  "pp";
    private static final String API_KEY_debug = "AIzaSyD8ohETeNCVDC25WBJzOS23flGSruVW9A0";
    private static final String API_KEY_release = "AIzaSyAPX3i67uLS9DIhRMcjgBe06KyaD6ucTy0";
    private static final short MAX_RESULTS = 50;
    public static final String CHANNEL_SRB = "UCV987OfHRz2G8k9ej_ePe0g";
    public static final String CHANNEL_HRV = "UCuJUvU9KPoQXpIirs8Z77jA";
    public static final String CHANNEL_BIH = "UCr-xEuuwWkAWhXlJFPPv6VQ";
    public static final String CHANNEL_SLO = "UCk9GJNzUtzVe-0KuLvXqnwg";
    public static final String CHANNEL_MKD = "UCwlaFTcP7lLwUixxsXIo6BQ";
    public static final String CHANNEL_MNE = "UCar7-RU5qp5J4bX1WDuqhyQ";

    public static final String AGE_1_2 = "1-2";
    public static final String AGE_3_4 = "3-4";
    public static final String AGE_5_6 = "5-6";

    public static final String LANG_SRB = "LANG_SRB";
    public static final String LANG_HRV = "LANG_HRV";
    public static final String LANG_BIH = "LANG_BIH";
    public static final String LANG_SLO = "LANG_SLO";
    public static final String LANG_MKD = "LANG_MKD";
    public static final String LANG_MNE = "LANG_MNE";

    public static ArrayList<String> getLangSelection(){
        ArrayList<String> channelIds = new ArrayList<>();
        if (CHANNEL_SRB.length()>0){channelIds.add(CHANNEL_SRB);}
        if (CHANNEL_HRV.length()>0){channelIds.add(CHANNEL_HRV);}
        if (CHANNEL_BIH.length()>0){channelIds.add(CHANNEL_BIH);}
        if (CHANNEL_SLO.length()>0){channelIds.add(CHANNEL_SLO);}
        if (CHANNEL_MKD.length()>0){channelIds.add(CHANNEL_MKD);}
        if (CHANNEL_MNE.length()>0){channelIds.add(CHANNEL_MNE);}
        return channelIds;
    }


    /** settings provera *************************** **/
    public static String[] getAccessSettingsQuestions(){
        String[][] accessSettings = {
                {"Upiši slovima broj 1:", "JEDAN"},
                {"Upiši slovima broj 7:", "SEDAM"},
                {"Upiši slovima broj 9:", "DEVET"}};
        int index = new Random().nextInt(accessSettings.length - 0);
        Log.w("accessSettings", "index "+index);
        return accessSettings[index];
    }


    public static short getClickToUnlockNum(){
        if (BuildConfig.IS_RELEASE)
            return 5;
        else
            return 3;
    }

    /** toast obavestenja i html string ***************************/
    private static Toast obavestenje;
    private static void prikaziToast_master(Context c, String s, int color, int gravity) {
        //"Toast toast" is declared in the class
        s = s.replaceAll("\n", "<br>");
        try {
            obavestenje.getView().isShown();     // true if visible
            obavestenje.setText(mojHtmlString(s));
        } catch (Exception e) {         // invisible if exception
            obavestenje = Toast.makeText(c, s, LENGTH_SHORT);
        }
        TextView tv = obavestenje.getView().findViewById(android.R.id.message);
        if( tv != null) { //
            tv.setGravity(Gravity.CENTER); // ovo je za centriranje teksta unutar toast
            tv.setTextColor(color);
        }
        if (gravity != Integer.MIN_VALUE)
            obavestenje.setGravity(gravity,0,0); //za centriranje toast na sredinu ekarana
        obavestenje.show();
    }
    public static void prikaziToast(Context c, String s) {
        prikaziToast_master(c,s, Color.YELLOW, Integer.MIN_VALUE);
    }
    public static void prikaziWarnToast(Context c, String s) {
        prikaziToast_master(c,s, Color.rgb(204,0,0), Gravity.CENTER);
    }
    public static Spanned mojHtmlString(String s) {
        // Verzije do API24 ne mogu da koriste Html.fromHtml(s, 0)
        // <b> BOLD </b>
        // <i> italic </i>
        // <u> underline </u>
        // <br> == \n
        // <font color='red'> fafadsnlgkjagnk </font>
        Spanned mSpanned;
        if (Build.VERSION.SDK_INT<24)
            mSpanned= Html.fromHtml(s);
        else
            mSpanned=Html.fromHtml(s, 0); // ne znam sta je 0

        return mSpanned;
    }

    /** opste f-je ***************************/
    public static int getIndex(String string, String[] array){
        for (int i = 0; i < array.length; i++)
            if (array[i].equals(string))
                return i;
        return 0;
    }

    public static String getLangFromChannelId(String channelID){
        String out="";
        switch (channelID){
            case CHANNEL_SRB:
                out= LANG_SRB;break;
            case CHANNEL_HRV:
                out= LANG_HRV;break;
            case CHANNEL_BIH:
                out= LANG_BIH;break;
            case CHANNEL_SLO:
                out= LANG_SLO;break;
            case CHANNEL_MKD:
                out= LANG_MKD;break;
            case CHANNEL_MNE:
                out= LANG_MNE;break;
        }
        return out;
    }

    public static String createApiUrl_Channel(String id){
        return "https://www.googleapis.com/youtube/v3/playlists?part=snippet&maxResults="+MAX_RESULTS+"&key="+getApiKey()+"&channelId="+id;
    }
    public static String createApiUrl_Playlist(String id){
        return "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults="+MAX_RESULTS+"&key="+getApiKey()+"&playlistId="+id;
    }

    public static void debugToast(Activity activity, String message){
        if ( ! BuildConfig.IS_RELEASE)
            Toast.makeText(activity, "DEBUG "+message, LENGTH_SHORT).show();
    }

    public static int odaberiAnimaciju_alertDialog(){
        ArrayList<Integer> listaAnimacijaDialoga = new ArrayList<>();
        //listaAnimacijaDialoga.add(R.style.DialogAnimation0);
        //listaAnimacijaDialoga.add(R.style.DialogAnimation1);
        listaAnimacijaDialoga.add(R.style.DialogAnimation_r2l);
        //listaAnimacijaDialoga.add(R.style.DialogAnimation3);
        return listaAnimacijaDialoga.get(new Random().nextInt(listaAnimacijaDialoga.size()));
    }


    /** inner-classes *********************************************/
    public static class Timings{
        protected Timings(){}

        public static final String TIMING_TOGGLE = "timing_toggle";

        public static final String _10SEC = "10s";
        public static final String _20SEC = "20s";
        public static final String _30SEC = "30s";
        public static final String _5MIN =   "5min";
        public static final String _10MIN = "10min";
        public static final String _15MIN = "15min";
        public static final String _20MIN = "20min";
        public static final String _30MIN = "30min";
        public static final String _45MIN = "45min";
        public static final String _1H =    "1h";
        public static final String _2H =    "2h";
        public static final String _3H =    "3h";
        public static final String _4H =    "4h";
        public static final String _5H =    "5h";

        private static final String[] debugTimings = {_10SEC, _20SEC, _5MIN, _15MIN, _1H, _5H};
        public static final String[]  getDayTimings(){
            String[] out;
            if (BuildConfig.IS_RELEASE){
                out = new String[]{_1H, _2H, _3H, _4H, _5H};
            }
            else{
                out = debugTimings;
            }
            return out;
        }
        public static final String[]  getNightTimings(){
            String[] out;
            if (BuildConfig.IS_RELEASE){
                out = new String[]{_30MIN, _45MIN, _1H, _2H};
            }
            else{
                out = debugTimings;
            }
            return out;
        }
        public static final String[]  getAtonceTimings(){
            String[] out;
            if (BuildConfig.IS_RELEASE){
                out = new String[]{_30MIN, _45MIN, _1H, _2H};
            }
            else{
                out = debugTimings;
            }
            return out;
        }


        public static final int NIGHT_BEGIN_HOUR = 20;
        public static final String TIME_COUNTER_DAY = "day";
        public static final String TIME_COUNTER_NIGHT = "night";
        public static final String TIME_COUNTER_ATONCE = "atonce";

        public static final String RESTRICTION_TIMING_DAY = "rest_day";
        public static final String RESTRICTION_TIMING_NIGHT = "rest_night";
        public static final String RESTRICTION_TIMING_ATONCE = "rest_atonce";

        public static int parseStringToSec(String string, int def){
            int temp = parseStringToSec(string);
            if (temp==0){
                temp = def;
            }
            return temp;
        }
        public static int parseStringToSec(String string){
            int temp;
            if (string.indexOf("h") != -1){
                temp = 60 * 60 * Integer.parseInt(string.replace("h",""));
            }
            else if (string.indexOf("min") != -1){
                temp = 60 * Integer.parseInt(string.replace("min",""));
            }
            else if (string.indexOf("s") != -1){
                temp = Integer.parseInt(string.replace("s",""));
            }
            else{
                temp =0;
            }
            return temp;
        }

    }

    public static class SharedPref{
        public SharedPref() {}

        /** global  SharedPreferences ***************************/
        private static SharedPreferences mSharedPreferences;
        public static SharedPreferences getSharPref(){
            return mSharedPreferences;
        }
        public static void setSharPref(SharedPreferences sp){
            mSharedPreferences =  sp;
        }

        public static boolean getBooleanSP(String string){
            return getSharPref().getBoolean(string,true);
        }
        public static boolean getBooleanSP(String string, boolean bool){
            return getSharPref().getBoolean(string,bool);
        }
        public static String getStringSP(String string){
            return getSharPref().getString(string,"");
        }
        public static void putBooleanSP(String string, Boolean bool){
            getSharPref().edit().putBoolean(string,bool).apply();
        }
        public static void putStringSP(String string, String data){
            getSharPref().edit().putString(string,data).apply();
        }

    }

    public static class Firebase{
        private FirebaseAnalytics mFirebaseAnalytics;
        //private YoutubeClass mYoutubeClass;
        private Activity mActivity;
        private  String logTypeDef;
        protected Firebase(Activity activity) {
            mActivity=activity;
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }

        public void sendYoutubeData(YoutubeClass youtubeClass, int postition){
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, youtubeClass.getId());
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, youtubeClass.getTitle());
            bundle.putInt("item_positon", postition);
            if (youtubeClass.getType()==YoutubeClass.VIDEO){
                logTypeDef = "selected_video";
            }
            else{
                bundle.putString("item_age", youtubeClass.getAge());
                bundle.putString("item_lang", youtubeClass.getLang());
                logTypeDef = "selected_playlist";
            }
            mFirebaseAnalytics.logEvent(logTypeDef, bundle);
            debugToast(mActivity,"logTypeDef->"+logTypeDef+" title->"+youtubeClass.getTitle());
        }

    }
}
