package nsp.youtubezadecu;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import static android.view.View.GONE;
import static nsp.youtubezadecu.FragmentPlaylists.filterPlaylists;
import static nsp.youtubezadecu.StaticData.SharedPref.putBooleanSP;
import static nsp.youtubezadecu.StaticData.SharedPref.putStringSP;
import static nsp.youtubezadecu.StaticData.Timings.getAtonceTimings;
import static nsp.youtubezadecu.StaticData.Timings.getDayTimings;
import static nsp.youtubezadecu.StaticData.Timings.getNightTimings;
import static nsp.youtubezadecu.StaticData.debugToast;
import static nsp.youtubezadecu.StaticData.prikaziToast;

public class FragmentSettings extends Fragment {

    private final String PLAY_STORE_LINK = "http://play.google.com/store/apps/details?id=" + getAppId();
    private final String EMAIL = "nikola.software.dev@gmail.com";

    private CheckBox mLangSRB;
    private CheckBox mLangHRV;
    private CheckBox mLangBIH;
    private CheckBox mLangSLO;
    private CheckBox mLangMKD;
    private CheckBox mLangMNE;
    private CheckBox mAge1_2;
    private CheckBox mAge3_4;
    private CheckBox mAge5_6;
    private Spinner mTimings;
    private Switch mSwitchTimings;
    private Spinner mTimingsDay;
    private Spinner mTimingsAtonce;
    private Spinner mTimingsNight;
    private Button mConfirmSettings;

    private static boolean langSRB;
    private static boolean langHRV;
    private static boolean langBIH;
    private static boolean langSLO;
    private static boolean langMKD;
    private static boolean langMNE;
    private static boolean age1_2;
    private static boolean age3_4;
    private static boolean age5_6;

    private static ArrayList<YoutubeClass> mYoutubePlaylistsList = new ArrayList<>();
    private Button mInfoSRB;
    private Button mInfoHRV;
    private Button mInfoBIH;
    private Button mInfoSLO;
    private Button mInfoMNE;
    private Button mInfoMKD;

    private String selectedTiming_Day, selectedTiming_Night, selectedTiming_Atonce;

    private LinearLayout mLinearLayout;
    private boolean timings_toggle;
    private Button mButtonOceni;
    private Button mButtonShare;
    private Button mButtonUslovi;
    private Button mButtonMail;

    public static FragmentSettings newInstance() {
        Bundle bundle = new Bundle();
        //bundle.putParcelableArrayList("youtubePlaylists", youtubePlaylists);
        FragmentSettings fragment = new FragmentSettings();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View view = inflater.inflate(R.layout.fragment_settings, viewGroup, false);
        //Bundle bundle = getArguments();
        //mYoutubePlaylistsList = bundle.getParcelableArrayList("youtubePlaylists");
        mYoutubePlaylistsList = YoutubeClass.getYT_playlists(getActivity());

        langSRB = StaticData.SharedPref.getBooleanSP(StaticData.LANG_SRB);
        langHRV = StaticData.SharedPref.getBooleanSP(StaticData.LANG_HRV);
        langBIH = StaticData.SharedPref.getBooleanSP(StaticData.LANG_BIH);
        langSLO = StaticData.SharedPref.getBooleanSP(StaticData.LANG_BIH);
        langMKD = StaticData.SharedPref.getBooleanSP(StaticData.LANG_MKD);
        langMNE = StaticData.SharedPref.getBooleanSP(StaticData.LANG_MNE);

        age1_2 = StaticData.SharedPref.getBooleanSP(StaticData.AGE_1_2);
        age3_4 = StaticData.SharedPref.getBooleanSP(StaticData.AGE_3_4);
        age5_6 = StaticData.SharedPref.getBooleanSP(StaticData.AGE_5_6);

        timings_toggle = StaticData.SharedPref.getBooleanSP(StaticData.Timings.TIMING_TOGGLE,false);

        selectedTiming_Day = StaticData.SharedPref.getStringSP(StaticData.Timings.RESTRICTION_TIMING_DAY);
        selectedTiming_Night = StaticData.SharedPref.getStringSP(StaticData.Timings.RESTRICTION_TIMING_NIGHT);
        selectedTiming_Atonce = StaticData.SharedPref.getStringSP(StaticData.Timings.RESTRICTION_TIMING_ATONCE);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar_settings);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        findViews(view);
        initializeViews(view);
        setListeners();

        Bundle bundle = new Bundle();
        bundle.putBoolean("settings_loaded_bundle", true);
        FirebaseAnalytics.getInstance(getActivity()).logEvent("settings_loaded", bundle);

    }

    private String getAppId(){
        //return getActivity().getPackageName();
        return BuildConfig.APPLICATION_ID;
    }
    private String getAppName(){
        ApplicationInfo applicationInfo = getActivity().getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : getActivity().getString(stringId);
    }

    public static void onClick_langs(View view, Activity activity) {
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()) {
            case R.id.langSRB:
                langSRB = checked;
                break;
            case R.id.langHRV:
                langHRV = checked;
                break;
            case R.id.langSLO:
                langSLO = checked;
                break;
            case R.id.langBIH:
                langBIH = checked;
                break;
            case R.id.langMKD:
                langMKD = checked;
                break;
            case R.id.langMNE:
                langMNE = checked;
                break;
        }
        debugToast(activity, ((CheckBox) view).getText().toString());
    }

    public static void onClick_ages(View view, Activity activity) {
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()) {
            case R.id.age1_2:
                age1_2 = checked;
                break;
            case R.id.age3_4:
                age3_4 = checked;
                break;
            case R.id.age5_6:
                age5_6 = checked;
                break;
        }
        debugToast(activity, ((CheckBox) view).getText().toString());
    }

    public static void clickInfoLangs(View view, Activity activity) {
        ArrayList<YoutubeClass> filteredPlaylists = new ArrayList<>();
        String message = "";
        switch (view.getId()) {
            case R.id.infoSRB:
                filteredPlaylists = filterByLang(StaticData.CHANNEL_SRB, mYoutubePlaylistsList);
                break;
            case R.id.infoHRV:
                filteredPlaylists = filterByLang(StaticData.CHANNEL_HRV, mYoutubePlaylistsList);
                break;
            case R.id.infoSLO:
                filteredPlaylists = filterByLang(StaticData.CHANNEL_SLO, mYoutubePlaylistsList);
                break;
            case R.id.infoBIH:
                filteredPlaylists = filterByLang(StaticData.CHANNEL_BIH, mYoutubePlaylistsList);
                break;
            case R.id.infoMKD:
                filteredPlaylists = filterByLang(StaticData.CHANNEL_MKD, mYoutubePlaylistsList);
                break;
            case R.id.infoMNE:
                message = "Trenutno nema niti jedan crtani na pred-vizantijskom jeziku.";
                //filteredPlaylists=filterByLang(CHANNEL_MNE, mYoutubePlaylistsList);
                break;
        }

        for (YoutubeClass item : filteredPlaylists) {
            message += item.getTitle() + "\n";
        }

        if (message.length() == 0) message = "Trenutno nema niti jednog snimka.";


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public static void clickInfoAges(View view, Activity activity) {
        ArrayList<YoutubeClass> filteredPlaylists = new ArrayList<>();
        String message = "";
        switch (view.getId()) {
            case R.id.info1_2:
                filteredPlaylists = filterByAge(StaticData.AGE_1_2, mYoutubePlaylistsList);
                break;
            case R.id.info3_4:
                filteredPlaylists = filterByAge(StaticData.AGE_3_4, mYoutubePlaylistsList);
                break;
            case R.id.info5_6:
                filteredPlaylists = filterByAge(StaticData.AGE_5_6, mYoutubePlaylistsList);
                break;
        }

        for (YoutubeClass item : filteredPlaylists) {
            message += item.getTitle() + "\n";
        }

        if (message.length() == 0) message = "Trenutno nema niti jednog snimka.";


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(message);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public static ArrayList<YoutubeClass> filterByLang(String channedID, ArrayList<YoutubeClass> allPlaylists) {
        ArrayList<YoutubeClass> filteredPlaylists = new ArrayList<>();
        for (YoutubeClass item : allPlaylists) {
            if (channedID.equals(item.getChannelId())) {
                filteredPlaylists.add(item);
            }
        }
        return filteredPlaylists;
    }

    public static ArrayList<YoutubeClass> filterByAge(String age, ArrayList<YoutubeClass> allPlaylists) {
        ArrayList<YoutubeClass> filteredPlaylists = new ArrayList<>();
        for (YoutubeClass item : allPlaylists) {
            if (age.equals(item.getAge())) {
                filteredPlaylists.add(item);
            }
        }
        return filteredPlaylists;
    }

    private void findViews(View view){
        mLangBIH = view.findViewById(R.id.langBIH);
        mLangHRV = view.findViewById(R.id.langHRV);
        mLangSRB = view.findViewById(R.id.langSRB);
        mLangSLO = view.findViewById(R.id.langSLO);
        mLangMKD = view.findViewById(R.id.langMKD);
        mLangMNE = view.findViewById(R.id.langMNE);
        mAge1_2 = view.findViewById(R.id.age1_2);
        mAge3_4 = view.findViewById(R.id.age3_4);
        mAge5_6 = view.findViewById(R.id.age5_6);
        mSwitchTimings = view.findViewById(R.id.switch_timings);
        mLinearLayout = view.findViewById(R.id.ll_timings);
        mTimingsDay = view.findViewById(R.id.timings_day);
        mTimingsAtonce = view.findViewById(R.id.timings_atonce);
        mTimingsNight = view.findViewById(R.id.timings_beforesleep);
        mConfirmSettings = view.findViewById(R.id.confirm_settings);
        mInfoSRB = view.findViewById(R.id.infoSRB);
        mInfoHRV = view.findViewById(R.id.infoHRV);
        mInfoBIH = view.findViewById(R.id.infoBIH);
        mInfoSLO = view.findViewById(R.id.infoSLO);
        mInfoMNE = view.findViewById(R.id.infoMNE);
        mInfoMKD = view.findViewById(R.id.infoMKD);
        mButtonOceni =  view.findViewById(R.id.button_oceni);
        mButtonShare =  view.findViewById(R.id.button_share);
        mButtonUslovi = view.findViewById(R.id.button_uslovi);
        mButtonMail =   view.findViewById(R.id.button_mail);
    }
    private void initializeViews(View view){
        if (mYoutubePlaylistsList.size() == 0) {
            view.findViewById(R.id.ll_langs).setVisibility(GONE);
            view.findViewById(R.id.ll_ages).setVisibility(GONE);
        }

        mLangSRB.setChecked(langSRB);
        mLangHRV.setChecked(langHRV);
        mLangBIH.setChecked(langBIH);
        mLangSLO.setChecked(langSLO);
        mLangMKD.setChecked(langMKD);
        mLangMNE.setChecked(langMNE);
        mAge1_2.setChecked(age1_2);
        mAge3_4.setChecked(age3_4);
        mAge5_6.setChecked(age5_6);

        mSwitchTimings.setChecked(timings_toggle);
        if (timings_toggle) {
            mLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mLinearLayout.setVisibility(GONE);
        }

        String[] day_list = getDayTimings();
        final ArrayAdapter<String> adapterDay = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, day_list);
        mTimingsDay.setAdapter(adapterDay);

        String[] night_list = getNightTimings();
        final ArrayAdapter<String> adapterNight = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, night_list);
        mTimingsNight.setAdapter(adapterNight);

        String[] atonce_list = getAtonceTimings();
        final ArrayAdapter<String> adapterAtOnce = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, atonce_list);
        mTimingsAtonce.setAdapter(adapterAtOnce);

        mTimingsDay.setSelection(StaticData.getIndex(selectedTiming_Day, day_list));
        mTimingsNight.setSelection(StaticData.getIndex(selectedTiming_Night, night_list));
        mTimingsAtonce.setSelection(StaticData.getIndex(selectedTiming_Atonce, atonce_list));

    }

    private void setListeners(){
        mSwitchTimings.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton compoundButton, boolean is_checked) {
                timings_toggle = is_checked;
                if (is_checked) {
                    mLinearLayout.setVisibility(View.VISIBLE);
                } else {
                    mLinearLayout.setVisibility(GONE);
                }
            }
        });

        mConfirmSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                putBooleanSP(StaticData.LANG_SRB, langSRB);
                putBooleanSP(StaticData.LANG_HRV, langHRV);
                putBooleanSP(StaticData.LANG_BIH, langBIH);
                putBooleanSP(StaticData.LANG_SLO, langSLO);
                putBooleanSP(StaticData.LANG_MKD, langMKD);
                putBooleanSP(StaticData.LANG_MNE, langMNE);
                putBooleanSP(StaticData.AGE_1_2, age1_2);
                putBooleanSP(StaticData.AGE_3_4, age3_4);
                putBooleanSP(StaticData.AGE_5_6, age5_6);
                putBooleanSP(StaticData.Timings.TIMING_TOGGLE, timings_toggle);
                putStringSP(StaticData.Timings.RESTRICTION_TIMING_DAY, selectedTiming_Day);
                putStringSP(StaticData.Timings.RESTRICTION_TIMING_NIGHT, selectedTiming_Night);
                putStringSP(StaticData.Timings.RESTRICTION_TIMING_ATONCE, selectedTiming_Atonce);

                if (getTotalSelectedPlaylists()==0){
                    prikaziToast(getActivity(),"Selekcijom nije obuhvaćen niti jedan crtani!");
                    return;
                }

                Bundle bundle = new Bundle();
                if(langSRB) bundle.putBoolean(StaticData.LANG_SRB, langSRB);
                if(langHRV) bundle.putBoolean(StaticData.LANG_HRV, langHRV);
                if(langBIH) bundle.putBoolean(StaticData.LANG_BIH, langBIH);
                if(langSLO) bundle.putBoolean(StaticData.LANG_SLO, langSLO);
                if(langMKD) bundle.putBoolean(StaticData.LANG_MKD, langMKD);
                if(langMNE) bundle.putBoolean(StaticData.LANG_MNE, langMNE);
                if(age1_2) bundle.putBoolean(StaticData.AGE_1_2, age1_2);
                if(age3_4) bundle.putBoolean(StaticData.AGE_3_4, age3_4);
                if(age5_6) bundle.putBoolean(StaticData.AGE_5_6, age5_6);
                if(timings_toggle) bundle.putBoolean(StaticData.Timings.TIMING_TOGGLE, timings_toggle);
                bundle.putString(StaticData.Timings.RESTRICTION_TIMING_DAY,    selectedTiming_Day   );
                bundle.putString(StaticData.Timings.RESTRICTION_TIMING_NIGHT,  selectedTiming_Night );
                bundle.putString(StaticData.Timings.RESTRICTION_TIMING_ATONCE, selectedTiming_Atonce);
                FirebaseAnalytics.getInstance(getActivity()).logEvent("selected_settings", bundle);

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);
            }
        });


        mTimingsDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedTiming_Day = adapterView.getSelectedItem().toString();
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        mTimingsNight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedTiming_Night = adapterView.getSelectedItem().toString();
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        mTimingsAtonce.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedTiming_Atonce = adapterView.getSelectedItem().toString();
            }
            @Override public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        mButtonMail.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Intent intentMail = new Intent(Intent.ACTION_SEND);
                intentMail.setType("message/rfc822");
                intentMail.putExtra(Intent.EXTRA_EMAIL, new String[]{EMAIL}); // the To mail.
                intentMail.putExtra(Intent.EXTRA_SUBJECT, getAppName());
                intentMail.putExtra(Intent.EXTRA_TEXT, "(verzija aplikacije: "+BuildConfig.VERSION_NAME+")\n\n");
                intentMail.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(Intent.createChooser(intentMail, "Pošalji mejl autoru aplikacije:"));
                } catch (android.content.ActivityNotFoundException ex) {
                    StaticData.prikaziToast(getActivity(), "Nije instaliran program za slanje mejlova!");
                }
            }
        });

        mButtonUslovi.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                prikaziUsloveKoriscenja(true);
            }
        });
        mButtonShare.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
                String shareMessage= "Android aplikacija za decu:\n";
                shareMessage = shareMessage + PLAY_STORE_LINK +"\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                try {
                    startActivity(Intent.createChooser(shareIntent, "Podeli link ka aplikaciji sa prijateljima:"));
                } catch(Exception e) {
                    StaticData.prikaziToast(getActivity(), "Nije dostupno...prijavite problem autoru!");
                }
            }
        });

        mButtonOceni.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Uri uri = Uri.parse("market://details?id=" + getAppId());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_STORE_LINK)));
                }
            }
        });
    }
    private void prikaziUsloveKoriscenja(final boolean bool_cancelable) {

        AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        //ad.getWindow().setWindowAnimations(odaberiAnimaciju_alertDialog());
        ad.setTitle(StaticData.mojHtmlString("<b>Uslovi korišćenja:"));
        ad.setIcon(R.mipmap.logo2_foreground);

        ad.setCancelable(bool_cancelable); //on back click
        ad.setCanceledOnTouchOutside(bool_cancelable); //on outside click

        String string_odrOdg = UsloviKoriscenja;

        final WebView webView = new WebView(getActivity());
        webView.loadData(string_odrOdg,  "text/html; charset=utf-8", "utf-8");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            webView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray, null));
        else
            webView.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));

        ad.setView(webView);

        ad.setButton(AlertDialog.BUTTON_POSITIVE, "PRIHVATAM",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        ad.setButton(AlertDialog.BUTTON_NEGATIVE, "ne prihvatam",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_UNINSTALL_PACKAGE);
                        intent.setData(Uri.parse("package:" + getAppId()));
                        startActivityForResult(intent, 111);
                        prikaziUsloveKoriscenja(bool_cancelable);
                    }
                });
        ad.show();

        ad.getButton(DialogInterface.BUTTON_NEGATIVE).setAllCaps(false);
        ad.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
        ad.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor( R.color.colorPrimary));
        ad.getWindow().setBackgroundDrawableResource(android.R.color.darker_gray);

    }

    private int getTotalSelectedPlaylists(){
        int out;
        ArrayList<YoutubeClass> selectedLists = filterPlaylists(mYoutubePlaylistsList);
        out = selectedLists.size();
        return out;
    }

    private final String UsloviKoriscenja =
            "<html><body>\n" +
                    "    <p>Potrebno je da pažlјivo pročitate sledeća Pravila i uslove kori&scaron;ćenja ove android aplikacije (u daljem tekstu:\n" +
                    "     Aplikacije), &nbsp;pre nego &scaron;to počnete da je koristitite. Kori&scaron;ćenjem ove Aplikacije se dobrovolјno i neopozivo sagla&scaron;avate sa sledćim pravilima i uslovima kori&scaron;ćenja. Ukoliko se ne slažete &ndash; nemojte koristiti Aplikaciju.</p> \n" +
                    "    <h3>Izuzeće od odgovornosti</h3> \n" +
                    "    <p>Aplikaciju koristite na sopstvenu odgovornost. Autor ove aplikacije (u daljem tekstu: Autor) nije odgovoran za materijalnu ili nematerijalnu &scaron;tetu, direktnu ili indirektnu, koja nastane pri kori&scaron;ćenju ili je u nekoj vezi sa kori&scaron;ćenjem Aplikacije ili njenog sadržaja.</p>\n" +
                    "    <p>Autor ne obezbeđuje nikakvu garanciju korisniku ove Aplikacije, niti omogućuje naknadu &scaron;tete korisniku nastalu kori&scaron;ćenjem ove Aplikacije ili njenog sadržaja.</p>\n" +
                    "    <p> Autor ima pravo da postavi limite i restrikcije na Aplikcaiji, kao i da oduzme pravo kori&scaron;ćenja Aplikacije bez obaveze da informi&scaron;e korisnika.</p>\n" +
                    "    <p>Sadržaj Aplikacije može sadržati netačne ili nepremerene podatke i multimedijalni sadržaj. Promene na Aplikaciji se mogu napraviti periodično, u bilo kom trenutku i bez obave&scaron;tenja. Autor&nbsp;se ne obavezuje da redovno ažurira informacije sadržane na Aplikaciji. Takođe, Autor&nbsp;ne garantuje da će Aplikacija funkcionisati bez prekida ili gre&scaron;aka, da će nedostaci biti blagovremeno ispravljani ili da je Aplikacija kompatibilna sa va&scaron;im hardverom ili softverom.</p>\n" +
                    "    <h3>Izmena uslova kori&scaron;ćenja</h3> \n" +
                    "    <p>Vlasnik Aplikacije zadržava pravo da dopuni ili izmeni sadržaj Aplikacije, ili bilo koje softverske alate na Aplikaciji, bez obaveze najave i obave&scaron;tenja korisnika.&nbsp;Pravila i uslovi kori&scaron;ćenja Aplikacije takođe mogu biti menjani bez prethodne najave i obave&scaron;tenja korisnika.</p>\n" +
                    "    <h3>Obaveze korisnika Aplikacija</h3> \n" +
                    "    <p> Korisnik Aplikacije je saglasan da ni u kom slučaju neće:</p>\n" +
                    "    <ul>\n" +
                    "    <li>kopirati, menjati, publikovati, distribuirati ili prodavati softverski kod Aplikacije;</li>\n" +
                    "    <li>koristiti Aplikaciju na način koji ima veze s piraterijom ili koji može za rezultat imati inficiranje Aplikacije softverskim virusima, zlonamenim ili bilo kojim drugim softverom;</li>\n" +
                    "    <li>pristupati, ni poku&scaron;avati pristup, delovima Aplikacije ili mreže za koje nije autorizovan.</li>\n" +
                    "    </ul>\n" +
                    "    <h3>Sadržaj koji se prikazuje u Aplikaciji</h3>\n" +
                    "    <p>Aplikacija omogućuje prikaz video snimaka preko Youtube kanala. Autor nije vlasnik tih snimika, niti ima kontrolu nad njima. Autor nije odgovoran za sadržaj tih snimaka, niti tačnost njihovih naziva/opisa.</p>\n" +
                    "    <p>Aplikacija može pratiti statističku posećenost delovima Aplikacije, isključivo radi pribavljanja nužnih informacija uz pomoć zvaničnih servisa Google Firebase analitike.</p>\n" +
                    "    </body></html>";
}
