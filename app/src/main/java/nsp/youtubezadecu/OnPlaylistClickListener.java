package nsp.youtubezadecu;

public interface OnPlaylistClickListener {
    void onPlaylistClick(YoutubeClass item, int position);
}