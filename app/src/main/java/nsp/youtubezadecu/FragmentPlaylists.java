package nsp.youtubezadecu;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.common.io.BaseEncoding;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import androidx.appcompat.app.AppCompatActivity;
import nsp.youtubezadecu.R;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cz.msebera.android.httpclient.Header;

import static nsp.youtubezadecu.StaticData.SharedPref.getBooleanSP;

public class FragmentPlaylists extends Fragment {
    private ArrayList<YoutubeClass> mYoutubePlaylistsList = new ArrayList<>();
    private ArrayList<YoutubeClass> mYoutubePlaylistsList_FILTERED = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private FirebaseAnalytics mFirebaseAnalytics;
    private StaticData.Firebase mFirebase;
    AlertDialog mAlertDialog;
    View mAlertDialogView;

    public static FragmentPlaylists newInstance(ArrayList<YoutubeClass> youtubePlaylists) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("youtubePlaylists", youtubePlaylists );
        FragmentPlaylists fragmentPlaylists = new FragmentPlaylists();
        fragmentPlaylists.setArguments(bundle);
        return fragmentPlaylists;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        MainActivity.testAtonceRestriction(getActivity());

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View view = inflater.inflate(R.layout.fragment_playlists, viewGroup, false);

        StaticData.debugToast(getActivity(), "FragmentPlaylists - onCreateView");

        FloatingActionButton floatingActionButton = view.findViewById(R.id.floating_action_button);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                clickedOnSettings();
            }
        });

        Bundle bundle = getArguments();
        mYoutubePlaylistsList = bundle.getParcelableArrayList("youtubePlaylists");
        mYoutubePlaylistsList_FILTERED = filterPlaylists(mYoutubePlaylistsList);

        recyclerView = view.findViewById(R.id.recycler_playlists);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        mFirebase = new StaticData.Firebase(getActivity());

        return view;
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar_playlist);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        mAdapter = new PlaylistAdapter(getActivity(), mYoutubePlaylistsList_FILTERED, new OnPlaylistClickListener() {
            @Override
            public void onPlaylistClick(YoutubeClass item, int postion) {
                //Toast.makeText(getActivity(), item.getTitle(), Toast.LENGTH_SHORT).show();
                String PlayList_ID = item.getId();
                String url = StaticData.createApiUrl_Playlist(PlayList_ID);
                httpRequest_videos(url, getActivity());
                mFirebase.sendYoutubeData(item, postion);
            }
        });
        recyclerView.setAdapter(mAdapter);


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getActivity());
        mAlertDialogView = layoutInflaterAndroid.inflate(R.layout.enter_settings_dialog, null);
        builder.setView(mAlertDialogView);
        builder.setCancelable(true);
        mAlertDialog= builder.create();
    }

    public static String getSHA1(String packageName, Activity activity){
        try {
            android.content.pm.Signature[] signatures = activity.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;
            for (android.content.pm.Signature signature: signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA-1");
                md.update(signature.toByteArray());
                return BaseEncoding.base16().encode(md.digest());
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
    private void httpRequest_videos(String url, final Activity act) {
        //Log.d("httpRequest_settings", "url: " + url);
        AsyncHttpClient httpClient = new AsyncHttpClient();

        String packageName = act.getPackageName();
        httpClient.addHeader("X-Android-Package", packageName);
        //Log.d("httpRequest_settings", "X-Android-Package: " + packageName);

        String sig = getSHA1(packageName, act);
        httpClient.addHeader("X-Android-Cert", sig);
        //Log.d("httpRequest_settings", "X-Android-Cert: " + sig);

        httpClient.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject jsonResponse) {
                //Log.d("httpRequest_settings", "onSuccess: " + jsonResponse.toString());
                ArrayList<YoutubeClass> youtubeVideoList = YoutubeClass.parseJsonResponse(jsonResponse);

                YoutubeFragment youtubeFragment = new YoutubeFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("youtubeVideoList", youtubeVideoList);
                youtubeFragment.setArguments(bundle);

                FragmentManager fragmentManager = (getActivity()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_home_placeholder, youtubeFragment);
                fragmentTransaction.addToBackStack("youtubeFragment");
                fragmentTransaction.commit();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject response) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                Log.d("httpRequest_settings", "Request fail! Status code: " + statusCode);
                Log.d("httpRequest_settings", "Fail response: " + response);
                Log.e("httpRequest_settings", "ERROR: " + e.toString());

                Toast.makeText(act, "Request Failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private short clicked = 0;
    private void clickedOnSettings() {
        if (clicked < StaticData.getClickToUnlockNum()) {
            StaticData.prikaziToast(getActivity(), "klikni brzo " + (StaticData.getClickToUnlockNum() - clicked) + " puta");
            clicked++;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    clicked = 0;
                }
            }, 2000);
        } else if (clicked == StaticData.getClickToUnlockNum()) {
            showDialogAfterClicking(getActivity(), mAlertDialog, mAlertDialogView, R.id.fragment_home_placeholder);
        }
    }
    public static void showDialogAfterClicking(final Activity activity, final AlertDialog alertDialog, View view, final int container) {
        alertDialog.show();

        TextView textView_qes = view.findViewById(R.id.question);
        final String[] ans_ques = StaticData.getAccessSettingsQuestions();
        textView_qes.setText(ans_ques[0]);

        final Button button = view.findViewById(R.id.submit);
        final EditText editText_ans = view.findViewById(R.id.answer);
        editText_ans.setText("");

        editText_ans.addTextChangedListener(new TextWatcher() {
            String answer;

            @Override public void onTextChanged(CharSequence cs, int s, int b, int c) {
                try {
                    answer = cs.toString();
                } catch (NumberFormatException e) {
                    answer = "";
                }
                //Log.w("accessSettings", "answer "+answer);
                if (answer.length() > 0 && answer.length() <= ans_ques[1].length()) {
                    button.setEnabled(true);
                } else if (answer.length() > ans_ques[1].length()) {
                    alertDialog.dismiss();
                } else {
                    button.setEnabled(false);
                }
            }
            @Override public void beforeTextChanged(CharSequence cs, int s, int b, int c) {}
            @Override public void afterTextChanged(Editable e) {}
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                String answer = editText_ans.getText().toString();
                if (answer.equals(ans_ques[1])) {
                    alertDialog.dismiss();
                    startSettingsFrag (activity, container);
                } else {
                    alertDialog.dismiss();
                    StaticData.prikaziWarnToast(activity, "NETAČNO!");
                }
            }
        });
    }
    public static void startSettingsFrag (Activity activity, int container){
        FragmentSettings fragmentSettings = FragmentSettings.newInstance();
        FragmentManager fragmentManager = ((AppCompatActivity)activity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(container, fragmentSettings);
        fragmentTransaction.addToBackStack("fragmentSettings");
        fragmentTransaction.commit();
    }

    public static ArrayList<YoutubeClass> filterPlaylists(ArrayList<YoutubeClass> allPlaylists){
        ArrayList<YoutubeClass> filteredPlaylists = new ArrayList<>();

        for (YoutubeClass item : allPlaylists) {
            if (checkLang(item.getChannelId()) && checkAge(item.getAge())){
                filteredPlaylists.add(item);
            }
        }
        return filteredPlaylists;
    }
    private static boolean checkLang(String channelID){
        if     (getBooleanSP(StaticData.LANG_SRB) && channelID.equals(StaticData.CHANNEL_SRB)) return true;
        else if(getBooleanSP(StaticData.LANG_HRV) && channelID.equals(StaticData.CHANNEL_HRV)) return true;
        else if(getBooleanSP(StaticData.LANG_BIH) && channelID.equals(StaticData.CHANNEL_BIH)) return true;
        else if(getBooleanSP(StaticData.LANG_SLO) && channelID.equals(StaticData.CHANNEL_SLO)) return true;
        else if(getBooleanSP(StaticData.LANG_MKD) && channelID.equals(StaticData.CHANNEL_MKD)) return true;
        else if(getBooleanSP(StaticData.LANG_MNE) && channelID.equals(StaticData.CHANNEL_MNE)) return true;
        else return false;
    }
    private static boolean checkAge(String age){
        if (age.length()==0) return false;

        if     (getBooleanSP(StaticData.AGE_1_2) && age.equals(StaticData.AGE_1_2)) return true;
        else if(getBooleanSP(StaticData.AGE_3_4) && age.equals(StaticData.AGE_3_4)) return true;
        else if(getBooleanSP(StaticData.AGE_5_6) && age.equals(StaticData.AGE_5_6)) return true;
        else return false;
    }

}
