package nsp.youtubezadecu;

import android.app.Activity;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static nsp.youtubezadecu.FragmentPlaylists.getSHA1;


/**
 * Nikola
 */
public class YoutubeClass implements Parcelable {
    public static final short VIDEO = 1;
    public static final short PLAYLIST = 2;

    private String description = "";
    private String title = "";
    private String publishedAt = "";
    private String thumbnail = "";
    private String id = "";
    private String channelId = "";
    private String age = "";
    private static boolean httpFired = false;
    private static boolean httpCompleted = false;

    private static ArrayList<YoutubeClass> sYoutubePlaylistsList = new ArrayList<>();
    public static ArrayList<YoutubeClass> getYT_playlists(Activity activity){
        if (sYoutubePlaylistsList.size()==0 && ! httpFired){
            httpFired = true;
            ArrayList<String> mLangSelectionChannels = StaticData.getLangSelection();
            for (String channelID :mLangSelectionChannels) {
                String url = StaticData.createApiUrl_Channel(channelID);
                httpRequest_playlists(url, mLangSelectionChannels.size(), activity);
            }
        }
        return sYoutubePlaylistsList;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    private String lang = "";

    private short type=0;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }


    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getId() {
        return id;
    }


    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(publishedAt);
        dest.writeString(thumbnail);
        dest.writeString(id);
        dest.writeString(channelId);
        dest.writeString(age);
    }


    public YoutubeClass() {
        super();
    }


    protected YoutubeClass(Parcel in) {
        this();
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.publishedAt = in.readString();
        this.thumbnail = in.readString();
        this.id = in.readString();
        this.channelId = in.readString();
        this.age = in.readString();

    }

    public static final Creator<YoutubeClass> CREATOR = new Creator<YoutubeClass>() {
        @Override
        public YoutubeClass createFromParcel(Parcel in) {
            return new YoutubeClass(in);
        }

        @Override
        public YoutubeClass[] newArray(int size) {
            return new YoutubeClass[size];
        }
    };


    private static short http_counter=0;
    private static ArrayList<YoutubeClass> tempYoutubePlaylistsList = new ArrayList<>();
    private static void httpRequest_playlists(String url, final int max_req, Activity activity ) {
        Log.d("httpRequest_settings", "url: " +url);
        AsyncHttpClient httpClient = new AsyncHttpClient();

        String packageName = activity.getPackageName();
        //debug - nsp.youtubezadecu.debug
        //release - nsp.youtubezadecu
        httpClient.addHeader("X-Android-Package", packageName);
        Log.d("httpRequest_settings", "X-Android-Package: " + packageName);

        String sig = getSHA1(packageName, activity);
        //debug  - E5186247F59A0BFEFD830BA77FE02294BD91AD12
        //relase - 3061A62F5156C931A8416BE0FFAAFBD1B83BF95F
        httpClient.addHeader("X-Android-Cert", sig);
        Log.d("httpRequest_settings", "X-Android-Cert: " + sig);

        httpClient.get(url, new JsonHttpResponseHandler() {
            @Override public void onSuccess(int statusCode, Header[] headers, JSONObject jsonResponse) {
                //Log.d("httpRequest", "onSuccess: " + jsonResponse.toString());
                tempYoutubePlaylistsList.addAll(parseJsonResponse(jsonResponse));

                http_counter++;
                if(http_counter==max_req){
                    httpFired=false;
                    sYoutubePlaylistsList.addAll(tempYoutubePlaylistsList);
                }
            }
            @Override public void onFailure(int statusCode, Header[] headers, Throwable e,JSONObject response) {
                Log.d("httpRequest", "Request fail! Status code: " + statusCode);
                Log.d("httpRequest", "Fail response: " + response);
                //Log.e("httpRequest", "ERROR: " + e.toString());
            }
        });
    }

    public static ArrayList<YoutubeClass> parseJsonResponse(JSONObject jsonObject) {
        ArrayList<YoutubeClass> mList = new ArrayList<>();

        if (jsonObject.has("items")) {
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("items");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject json = jsonArray.getJSONObject(i);
                    if (json.has("id")) {
                        if (json.has("snippet")) {
                            YoutubeClass youtubeObject = new YoutubeClass();
                            JSONObject jsonSnippet = json.getJSONObject("snippet");
                            String title = jsonSnippet.getString("title");
                            String description = jsonSnippet.getString("description");
                            String channelId = jsonSnippet.getString("channelId");
                            String publishedAt = jsonSnippet.getString("publishedAt");
                            String thumbnail = jsonSnippet.getJSONObject("thumbnails").getJSONObject("high").getString("url");

                            youtubeObject.setTitle(title);
                            youtubeObject.setDescription(description);
                            youtubeObject.setPublishedAt(publishedAt.substring(0, Math.min(publishedAt.length(), 10)));
                            youtubeObject.setThumbnail(thumbnail);
                            youtubeObject.setChannelId(channelId);
                            String id;
                            if (json.getString("kind").equals("youtube#playlist")) {
                                youtubeObject.setType(YoutubeClass.PLAYLIST);
                                id = json.getString("id");
                                String age = extractDataFromString(description);
                                youtubeObject.setAge(age);
                                youtubeObject.setLang(StaticData.getLangFromChannelId(channelId));
                            }
                            else{
                                youtubeObject.setType(YoutubeClass.VIDEO);
                                JSONObject resourceId = jsonSnippet.getJSONObject("resourceId");;
                                id = resourceId.getString("videoId");
                            }
                            youtubeObject.setId(id);

                            mList.add(youtubeObject);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return mList;
    }


    private static String extractDataFromString(String desc){
        String out;
        try {
            out=desc.substring(desc.indexOf("[")+1,desc.indexOf("]"));
        }catch (Exception e){
            out="";
        }
        return out;
    }


}
