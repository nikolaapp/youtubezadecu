package nsp.youtubezadecu;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Calendar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import static nsp.youtubezadecu.Fragment_restrictriction.REST_ATONCE;
import static nsp.youtubezadecu.Fragment_restrictriction.REST_DAY;
import static nsp.youtubezadecu.Fragment_restrictriction.REST_NIGHT;
import static nsp.youtubezadecu.StaticData.debugToast;


public class MainActivity extends AppCompatActivity {
    private static long startTime;
    private int CURENT_HOUR;
    private static ArrayList<YoutubeClass> mYoutubePlaylistsList = new ArrayList<>();
    private int welcomeDialogTrack=0;
    ProgressBar mProgressBar;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toast.makeText(this, getPackageName(), LENGTH_SHORT).show();
        //Toast.makeText(this, getSHA1(getPackageName()), LENGTH_SHORT).show();

        StaticData.SharedPref.setSharPref(getSharedPreferences("GLOBAL_DATA", MODE_PRIVATE));
        startTime =  System.currentTimeMillis();
        CURENT_HOUR = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

        if(StaticData.SharedPref.getBooleanSP(StaticData.PRVO_POKRETANJE)){
            debugToast(this, "inicijalizacija...");
            StaticData.SharedPref.putBooleanSP(StaticData.PRVO_POKRETANJE,false);
            welcomeDialog(false,this, title_array[welcomeDialogTrack], message_array[welcomeDialogTrack], R.style.DialogAnimation_first);
        }
        else{
            debugToast(this, "nije prvo pokretanje");
        }

        mProgressBar = findViewById(R.id.progressBar);


        startFrag(1000);
        startFrag(2000);
        startFrag(5000);
        startFrag(10000);

    }

    private boolean fragmentCreated = false;
    private Handler startFrag(final int time){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override public void run() {
                //debugToast(MainActivity.this,"tryFrag "+time);
                mYoutubePlaylistsList = YoutubeClass.getYT_playlists(MainActivity.this);
                if (mYoutubePlaylistsList.size()>0 && ! fragmentCreated){
                    fragmentCreated = true;
                    mProgressBar.setVisibility(View.GONE);
                    if(testRestriction()){
                        debugToast(MainActivity.this,"time "+time);
                        FragmentPlaylists fragmentPlaylists = FragmentPlaylists.newInstance(mYoutubePlaylistsList);
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.fragment_home_placeholder, fragmentPlaylists);
                        //ft.addToBackStack("fragmentPlaylists");
                        ft.commit();
                    }

                }
            }
        }, time);
        return handler;
    }

    @Override protected void onStart() {
        super.onStart();
    }
    @Override protected void onPause() {
        int elapsedTime = (int) (System.currentTimeMillis() - startTime)/(1000);
        debugToast(this,"onPause elapsedTime "+elapsedTime);
        if (elapsedTime>0){
            int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

            //todo sta je sa prelazom dan/noc
            if (currentHour< StaticData.Timings.NIGHT_BEGIN_HOUR){
                String previousTimeString = StaticData.SharedPref.getSharPref().getString(StaticData.Timings.TIME_COUNTER_DAY,"");
                int previousTime = stringToTime(previousTimeString);
                String out = timeToString(elapsedTime+previousTime);
                StaticData.SharedPref.getSharPref().edit().putString(StaticData.Timings.TIME_COUNTER_DAY,out).apply();
            }
            else{
                String previousTimeString = StaticData.SharedPref.getSharPref().getString(StaticData.Timings.TIME_COUNTER_NIGHT,"");
                int previousTime = stringToTime(previousTimeString);
                String out = timeToString(elapsedTime+previousTime);
                StaticData.SharedPref.getSharPref().edit().putString(StaticData.Timings.TIME_COUNTER_NIGHT,out).apply();
            }

            String temp = StaticData.SharedPref.getSharPref().getString(StaticData.Timings.TIME_COUNTER_DAY,"empty!!!!!");
            Log.e("timing", StaticData.Timings.TIME_COUNTER_DAY + " " + temp);
            temp = StaticData.SharedPref.getSharPref().getString(StaticData.Timings.TIME_COUNTER_NIGHT,"empty!!!!!");
            Log.e("timing", StaticData.Timings.TIME_COUNTER_NIGHT + " " + temp);

        }

        super.onPause();
    }

    /**
     * true - no restriction
     * false - rest detected and new activity fired
     */
    private boolean testRestriction(){
        if (! StaticData.SharedPref.getBooleanSP(StaticData.Timings.TIMING_TOGGLE, false)){
            return true;
        }

        boolean out = true;
        long nextScheadlue = StaticData.SharedPref.getSharPref().getLong(StaticData.Timings.TIME_COUNTER_ATONCE,0);

        if (startTime<nextScheadlue){
            out = false;
            startNewAct(this,REST_ATONCE);
        }
        else if (CURENT_HOUR< StaticData.Timings.NIGHT_BEGIN_HOUR){
            String previousTimeString = StaticData.SharedPref.getSharPref().getString(StaticData.Timings.TIME_COUNTER_DAY,"");
            int previousUsingTime = stringToTime(previousTimeString);
            int maxTime = StaticData.Timings.parseStringToSec( StaticData.SharedPref.getStringSP(StaticData.Timings.RESTRICTION_TIMING_DAY),3600);
            if (previousUsingTime>maxTime){
                out = false;
                startNewAct(this, REST_DAY);
            }
        }
        else if (CURENT_HOUR>= StaticData.Timings.NIGHT_BEGIN_HOUR){
            String previousTimeString = StaticData.SharedPref.getSharPref().getString(StaticData.Timings.TIME_COUNTER_NIGHT,"");
            int previousUsingTime = stringToTime(previousTimeString);
            int maxTime = StaticData.Timings.parseStringToSec( StaticData.SharedPref.getStringSP(StaticData.Timings.RESTRICTION_TIMING_NIGHT),3600);
            if (previousUsingTime>maxTime){
                out = false;
                startNewAct(this, REST_NIGHT);
            }
        }
        return out;
    }

    public void onClick_langs(View view) {
        FragmentSettings.onClick_langs(view, this);
    }
    public void onClick_ages(View view) {
            FragmentSettings.onClick_ages(view, this);
    }
    public void clickInfoLangs(View view) {
        FragmentSettings.clickInfoLangs(view, this);
    }
    public void clickInfoAges(View view) {
        FragmentSettings.clickInfoAges(view, this);
    }



    private static int stringToTime(String string){
        int time;
        String[] savedData = string.split("]");
        savedData[0]+="]";
        if (savedData[0].equals(currentDate())){
            time = Integer.parseInt(savedData[1]);
        }
        else{
            time=0;
        }
        return time;
    }
    private String timeToString(int time){
        String out = currentDate() + time;
        return out;
    }
    private static String currentDate(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int day = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
        return "["+year+""+day+"]";
    }

    public static void startNewAct(Activity activity, short selector){
        Intent myIntent = new Intent(activity, RestrictionActivity.class);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        myIntent.putExtra("selector", selector);
        myIntent.putParcelableArrayListExtra("mYoutubePlaylistsList", mYoutubePlaylistsList);
        activity.startActivity(myIntent);
        activity.finish();
    }

    public static void  testAtonceRestriction(Activity activity){
        if (! StaticData.SharedPref.getBooleanSP(StaticData.Timings.TIMING_TOGGLE, false)){
            return;
        }

        int elapsedTime = (int) (System.currentTimeMillis() - startTime)/(1000);
        int maxTime = StaticData.Timings.parseStringToSec( StaticData.SharedPref.getStringSP(StaticData.Timings.RESTRICTION_TIMING_ATONCE),600);
        if (elapsedTime>=maxTime){
            long nextTime = System.currentTimeMillis() + maxTime*1000;
            StaticData.SharedPref.getSharPref().edit().putLong(StaticData.Timings.TIME_COUNTER_ATONCE,nextTime).apply();
            startNewAct(activity, REST_ATONCE);
        }
    }

    private void welcomeDialog(final boolean bool_cancelable , final Activity activity, final String title, final String message, int animation) {
        final AlertDialog ad = new AlertDialog.Builder(activity).create();
        ad.getWindow().setWindowAnimations(animation);
        ad.setTitle(StaticData.mojHtmlString("<b>"+title+"</b>"));
        ad.setIcon(R.mipmap.logo2_foreground);

        ad.setCancelable(bool_cancelable); //on back click
        ad.setCanceledOnTouchOutside(bool_cancelable); //on outside click

        final WebView webView = new WebView(activity);
        webView.loadData(message,  "text/html; charset=utf-8", "utf-8");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            webView.setBackgroundColor(getResources().getColor(R.color.colorAccent, null));
        else
            webView.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        ad.setMessage(message);

        String next="dalje";
        if (welcomeDialogTrack==title_array.length-1){
            next = "OK";
        }
        ad.setButton(AlertDialog.BUTTON_POSITIVE, next,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //prikaziToast(activity, "BUTTON_POSITIVE" );
                        ad.dismiss();
                        clickedNext();
                    }
                });

        if (welcomeDialogTrack!=0){
            ad.setButton(AlertDialog.BUTTON_NEGATIVE, "nazad",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //prikaziToast(activity, "BUTTON_NEGATIVE" );
                            ad.dismiss();
                            clickedPrev();
                        }
                    });
        }
        ad.show();

        ad.getButton(DialogInterface.BUTTON_NEGATIVE).setAllCaps(false);
        ad.getButton(DialogInterface.BUTTON_POSITIVE).setAllCaps(false);

        ad.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimaryLight));
        ad.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimaryDark));

        ad.getWindow().setBackgroundDrawableResource(R.color.colorAccent);

    }

    private void clickedNext(){
        if (welcomeDialogTrack == title_array.length-1){
            welcomeDialogTrack=0;
        }
        else{
            welcomeDialogTrack++;
            welcomeDialog(false,this, title_array[welcomeDialogTrack], message_array[welcomeDialogTrack], R.style.DialogAnimation_r2l);
        }
    }
    private void clickedPrev(){
        if (welcomeDialogTrack > 0){
            welcomeDialogTrack--;
            welcomeDialog(false,this, title_array[welcomeDialogTrack], message_array[welcomeDialogTrack], R.style.DialogAnimation_l2r);
        }
    }

    private final String[] title_array = {
            "Dobrodošli",
            "Čemu applikacija služi",
            "Kakva je korist od nje",
            "Koji se crtani filmovi prikazuju",
            "Da li su liste crtanih filmova proverene",
            "Još nešto"
    };
    private final String[] message_array = {
            "Potrebno je da pažljivo pročitajte sledeće poruke kako bi se informisali o aplikaciji...",
            "Aplikacija je namenjana maloj deci radi gledanja domaćih crtanih filmova sa odabranih Youtube kanala – tako da deca ne mogu da pristupe drugim video snimcima!",
            "Gledajući samo crtaće na našem jeziku, deca će imati veće šanse da nauče reči našeg jezika – a ne da im prve reči budu na engleskom, kineskom…",
            "Crtaći koji se prikazuju se nalaze na Youtube listama koje je kreirao autora ove aplikacije. U podešavanjima aplikacije staraoci dece treba da odaberu kojim listama deca mogu da pristupe.",
            "Autor aplikacije ne garantuje da su crtani filmovi prilagodjeni za vašu decu – STARATELJI DECE SU U OBAVEZI DA KONTROLIŠU SADRŽAJ KOJEM DECA PRISTUPAJU PREKO OVE APLIKACIJE!  Ako misle da sadržaj nije primeren deci – u obavezi su da aplikaciju OBRIŠU, ili bar da ne dozvole deci da je koriste!",
            "U podešavanima aplikacije omogućeno ja da se ograniči koliko vremena aplikacija može da se koristi u toku izabranog vremenskog perioda… TAKOĐE, korišćenjem aplikacije saglasni ste sa uslovima korišćenja navedeni u podešavanjima."
    };

}
